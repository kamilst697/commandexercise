public class RotateCommand implements Command<LineSegment> {
    private double angle;
    private double x;
    private double y;

    public RotateCommand(double angle, double x, double y) {
        this.angle = angle;
        this.x = x;
        this.y = y;
    }

    @Override
    public void execute(LineSegment lineSegment) {
        lineSegment.rotate(angle, x, y);
    }
}
