public class TranslateCommand implements Command<LineSegment> {
    private double x;
    private double y;

    public TranslateCommand(double x, double y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public void execute(LineSegment lineSegment) {
        lineSegment.translate(x, y);
    }
}
