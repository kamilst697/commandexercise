public class Main {
    public static void main(String[] args) {
        LineSegment lineSegment = new LineSegment(1, 1, 1, 1);
        Invoker invoker = new Invoker();
        invoker.translate(new TranslateCommand(1, 1), lineSegment);
        invoker.rotate(new RotateCommand(90, 0, 0), lineSegment);
        lineSegment.showPoints();
    }
}