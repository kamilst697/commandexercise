public class Invoker {
    public void changePoints(ChangePointsCommand command, LineSegment lineSegment) {
        command.execute(lineSegment);
    }

    public void translate(TranslateCommand command, LineSegment lineSegment) {
        command.execute(lineSegment);
    }

    public void rotate(RotateCommand command, LineSegment lineSegment) {
        command.execute(lineSegment);
    }
}