public class LineSegment {
    private double x1, x2, y1, y2;

    public LineSegment(int x1, int x2, int y1, int y2) {
        this.x1 = x1;
        this.x2 = x2;
        this.y1 = y1;
        this.y2 = y2;
    }

    public void showPoints() {
        System.out.printf("Point A: %s, %s%n", x1, y1);
        System.out.printf("Point B: %s, %s%n", x2, y2);
    }

    public void changePoints(double newX1, double newX2, double newY1, double newY2) {
        this.x1 = newX1;
        this.x2 = newX2;
        this.y1 = newY1;
        this.y2 = newY2;
    }

    public void rotate(double angle, double x, double y) {
        double radians = Math.toRadians(angle);
        double cos = Math.cos(radians);
        double sin = Math.sin(radians);

        double newX1 = cos * (x1 - x) - sin * (y1 - y) + x;
        double newY1 = sin * (x1 - x) + cos * (y1 - y) + y;
        double newX2 = cos * (x2 - x) - sin * (y2 - y) + x;
        double newY2 = sin * (x2 - x) + cos * (y2 - y) + y;

        x1 = newX1;
        x2 = newX2;
        y1 = newY1;
        y2 = newY2;
    }

    public void translate(double dx, double dy) {
        x1 += dx;
        x2 += dx;
        y1 += dy;
        y2 += dy;
    }
}
