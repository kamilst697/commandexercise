public class ChangePointsCommand implements Command<LineSegment> {
    private double x1;
    private double x2;
    private double y1;
    private double y2;

    public ChangePointsCommand(double x1, double x2, double y1, double y2) {
        this.x1 = x1;
        this.x2 = x2;
        this.y1 = y1;
        this.y2 = y2;
    }

    @Override
    public void execute(LineSegment lineSegment) {
        lineSegment.changePoints(x1, x2, y1, y2);
    }
}
